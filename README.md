Webpack + React + Babel + Mocha
===

Contador que incrementa seu valor em 1 a cada segundo.
Projeto utilizado para iniciar o aprendizado com React.  

### Usage

```
npm install && npm start
open http://localhost:8080
```
