var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var path = require('path');
var resolve = function (dir) {
    return path.resolve(__dirname, dir);
};

module.exports = {
    entry: [
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:8080',
        resolve('app/main.js'),
    ],
    output: {
        path: resolve('build'),
        filename: 'bundle.js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
         append: true,
         template: path.join(__dirname, 'build/index.html')
     })
    ],
    module: {
        loaders: [{
          test: /\.js$/, // Transform all .js files required somewhere with Babel
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
              presets: ["babel-preset-es2015", "babel-preset-stage-0"].map(require.resolve),
          },
        }],
    }
};
