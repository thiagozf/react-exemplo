// @flow

import React, { Component, PropTypes } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  text-align: center;
`

const Numeros = styled.div`
  font-size: 72pt;
  color: #ffffff;
`

const Descricao = styled.span`
  width: 100%;
  font-size: 36pt;
  color: #ffffff;
  text-align: center;
`

type Props = {
  cor: string,
};

type State = {
  count: number,
}

class Contador extends Component {
  static displayName = 'Contador';

  state: State;
  props: Props;

  static defaultProps: { cor: string };

  state = {
    count: 0,
  }

  componentDidMount() {
    setInterval(this.count, 1000);
  }

  count = () => {
    const { count } = this.state;
    const teste = {};
    this.setState({
      count: count + 1
    });
  };

  render() {
    const {
      count,
    } = this.state;

    return (
      <Container>
        <Numeros>{`${count}`}</Numeros>
        <Descricao>segundos</Descricao>
      </Container>
    )
  }
}

Contador.defaultProps = { cor: '#ff0000' };

export default Contador;
