// import React from 'react';
// import ReactDOM from 'react-dom';
//
// import Contador from './contador';
//
// ReactDOM.render(
//   <Contador />,
//   document.getElementById('app')
// )
import React from 'react'
import { render } from 'react-dom'
import App from './App'

import FontFaceObserver from 'fontfaceobserver';

import './global-styles';

const openSansObserver = new FontFaceObserver('Open Sans', {});

openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
}, () => {
  document.body.classList.remove('fontLoaded');
});

render(
  <App />,
  document.getElementById('app')
)
