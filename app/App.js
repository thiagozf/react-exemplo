// @flow

import React, { Component } from 'react'
import styled from 'styled-components';
import Contador from './Contador'


export default class App extends Component {
  static displayName = 'App';

  render() {
    return (
      <Contador />
    )
  }
}
