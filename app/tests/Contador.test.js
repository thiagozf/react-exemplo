import React from 'react';

import { shallow, mount } from 'enzyme';
import Contador from '../Contador';

jest.useFakeTimers();

describe('Contador', function () {
  it('should render', function () {
    const wrapper = shallow(<Contador />);
    expect(wrapper).toBeDefined();
  });

  it('should count each second', function () {
    const wrapper = mount(<Contador />);
    expect(setInterval.mock.calls.length).toBe(1);
    expect(setInterval.mock.calls[0][0]).toBe(wrapper.instance().count);
    expect(setInterval.mock.calls[0][1]).toBe(1000);
  });

  it('should count', function () {
    const wrapper = mount(<Contador />);
    expect(wrapper.state().count).toBe(0);
    wrapper.instance().count();
    expect(wrapper.state().count).toBe(1);
  });
})
